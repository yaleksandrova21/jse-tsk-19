package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Comparator;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project findByName(String name) {
        for (Project project : list)
            if (name.equals(project.getName())) return project;
        return null;
    }

    @Override
    public Project removeByName(String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project startByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startById(String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByName(String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

}
