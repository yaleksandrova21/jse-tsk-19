package ru.yaleksandrova.tm.command.user;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.exception.system.AccessDeniedException;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class UserChangePasswordCommand extends AbstractCommand {
    @Override
    public String name() {
        return "user-change-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change user password";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD :");
        final String password = ApplicationUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}
