package ru.yaleksandrova.tm.command.system;

import ru.yaleksandrova.tm.command.AbstractCommand;

public final class VersionDisplayCommand extends AbstractCommand {

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Display program version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}
