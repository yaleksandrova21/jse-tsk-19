package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("ENTER ID:");
        final String id = ApplicationUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeById(id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        System.out.println("[Task deleted]");
        showTask(task);
    }
}
