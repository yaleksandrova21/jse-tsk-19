package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskStartByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-start-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start task by name";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        final Task task = serviceLocator.getTaskService().startByName(name);
        if(task == null) {
            throw new TaskNotFoundException();
        }
    }

}
