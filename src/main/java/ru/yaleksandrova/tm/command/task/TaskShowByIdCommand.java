package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-show-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by id";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(id);
        if (task == null){
            throw new TaskNotFoundException();
        }
        showTask(task);
    }

}
