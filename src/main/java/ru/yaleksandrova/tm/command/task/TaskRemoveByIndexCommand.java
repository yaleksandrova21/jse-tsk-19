package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by index";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER INDEX]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = serviceLocator.getTaskService().removeByIndex(index);
        if (task == null){
            throw new TaskNotFoundException();
        }
        System.out.println("[Task deleted]");
        showTask(task);
    }
}
