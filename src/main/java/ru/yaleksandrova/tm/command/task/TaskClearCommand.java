package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear all tasks";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        serviceLocator.getTaskService().clear();
        System.out.println("[OK]");
    }

}
