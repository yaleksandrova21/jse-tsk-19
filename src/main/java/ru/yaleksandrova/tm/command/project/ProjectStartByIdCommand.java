package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectStartByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-start-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by id";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startById(id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
    }
}
