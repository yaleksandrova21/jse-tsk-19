package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractProjectCommand;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by name";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeByName(name);
        if (project==null){
            throw new ProjectNotFoundException();
        }
        System.out.println("[Project deleted]");
        showProject(project);
    }

}
