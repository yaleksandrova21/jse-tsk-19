package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractProjectCommand;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by id";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(id);
        if (project==null){
            throw new ProjectNotFoundException();
        }
        showProject(project);
    }

}
