package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectChangeStatusByNameCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-change-status-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change project status by name";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = serviceLocator.getProjectService().changeStatusByName(name, status);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        serviceLocator.getProjectService().changeStatusByName(name, status);
    }
}
