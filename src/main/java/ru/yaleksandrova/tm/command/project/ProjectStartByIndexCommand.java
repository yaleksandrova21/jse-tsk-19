package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectStartByIndexCommand extends AbstractCommand {
    @Override
    public String name() {
        return "project-start-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by index";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER INDEX]");
        final Integer index = Integer.parseInt(ApplicationUtil.nextLine());
        final Project project = serviceLocator.getProjectService().startByIndex(index);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
    }
}
