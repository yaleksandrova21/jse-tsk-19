package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectFinishByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by id";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishById(id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        serviceLocator.getProjectService().finishById(id);
    }

}
