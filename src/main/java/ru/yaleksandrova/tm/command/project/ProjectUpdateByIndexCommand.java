package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractProjectCommand;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by index";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER INDEX]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Project project = serviceLocator.getProjectService().findByIndex(index);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        final Project projectUpdatedIndex = serviceLocator.getProjectService().updateByIndex(index, name, description);
        if (projectUpdatedIndex == null) {
            return;
        }
        System.out.println("[Project updated]");
    }

}
