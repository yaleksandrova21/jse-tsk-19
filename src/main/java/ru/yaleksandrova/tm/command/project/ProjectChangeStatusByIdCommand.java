package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectChangeStatusByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-change-status-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change project status by id";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = serviceLocator.getProjectService().changeStatusById(id, status);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        serviceLocator.getProjectService().changeStatusById(id, status);
    }
}
