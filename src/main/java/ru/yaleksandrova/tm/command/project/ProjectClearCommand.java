package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractCommand;

public final class ProjectClearCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear all projects";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        serviceLocator.getProjectService().clear();
        System.out.println("[OK]");
    }

}
