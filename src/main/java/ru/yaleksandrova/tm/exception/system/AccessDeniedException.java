package ru.yaleksandrova.tm.exception.system;

import ru.yaleksandrova.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Acess Denied!");
    }

}
