package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Task;
import java.util.List;
import java.util.Comparator;

public interface ITaskRepository extends IRepository<Task> {

    Task findByName(String name);

    Task removeByName(String name);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task startById(String id);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    Task bindTaskToProjectById(String projectId, String taskId);

    List<Task> findAllTaskByProjectId(String projectId);

    Task unbindTaskById(String taskId);

    void removeAllTaskByProjectId(String projectId);

}
