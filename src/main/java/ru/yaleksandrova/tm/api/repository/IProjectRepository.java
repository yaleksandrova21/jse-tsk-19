package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;
import java.util.List;
import java.util.Comparator;

public interface IProjectRepository extends IRepository<Project> {

    Project findByName(String name);

    Project removeByName(String name);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project startById(String id);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

}
