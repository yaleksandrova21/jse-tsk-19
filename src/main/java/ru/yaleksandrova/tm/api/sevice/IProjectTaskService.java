package ru.yaleksandrova.tm.api.sevice;

import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String projectId);

    Task bindTaskById(String projectId, String taskId);

    Task unbindTaskById(String projectId, String taskId);

    void removeAllTaskByProjectId(String projectId);

    Project removeById(String projectId);

}
