package ru.yaleksandrova.tm.service;

import ru.yaleksandrova.tm.api.repository.ITaskRepository;
import ru.yaleksandrova.tm.api.sevice.ITaskService;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.empty.EmptyIdException;
import ru.yaleksandrova.tm.exception.empty.EmptyIndexException;
import ru.yaleksandrova.tm.exception.empty.EmptyNameException;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.exception.system.IndexIncorrectException;
import ru.yaleksandrova.tm.model.Task;

import java.util.List;
import java.util.Comparator;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }


    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty())  throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public Task findByName(String name) {
        if(name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(name);
    }


    @Override
    public Task updateById(String id, String name, String description) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Task task = findById(id);
        if (task == null)
            throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null) throw new EmptyIndexException();
        if(index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }


    @Override
    public Task startById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.startById(id);
    }

    @Override
    public Task startByIndex(Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0) throw new IndexIncorrectException();
        return taskRepository.startByIndex(index);
    }

    @Override
    public Task startByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.startByName(name);
    }

    @Override
    public Task finishById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.finishById(id);
    }

    @Override
    public Task finishByIndex(Integer index) {
        if (index == null) throw new EmptyIndexException();
        if(index < 0) throw new IndexIncorrectException();
        return taskRepository.finishByIndex(index);
    }

    @Override
    public Task finishByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.finishByName(name);
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) return null;
        return taskRepository.changeStatusById(id, status);
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status) {
        if (index == null) throw new EmptyIndexException();
        if(index < 0) throw new IndexIncorrectException();
        if (status == null) return null;
        return taskRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Task changeStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) return null;
        return taskRepository.changeStatusByName(name, status);
    }

}
