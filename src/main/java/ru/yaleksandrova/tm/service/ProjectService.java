package ru.yaleksandrova.tm.service;

import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.api.sevice.IProjectService;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.empty.EmptyIdException;
import ru.yaleksandrova.tm.exception.empty.EmptyIndexException;
import ru.yaleksandrova.tm.exception.empty.EmptyNameException;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;

import java.util.Collections;
import java.util.List;
import java.util.Comparator;


public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public Project findByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Project project = findById(id);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Project project = findByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeByName(String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.removeByName(name);
    }


    @Override
    public Project startByIndex(Integer index) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.startByName(name);
    }

    @Override
    public Project startById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.startById(id);
    }

    @Override
    public Project finishById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(Integer index) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.finishByName(name);
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) return null;
        return projectRepository.changeStatusById(id, status);
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) return null;
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) return null;
        return projectRepository.changeStatusByName(name, status);
    }

}
